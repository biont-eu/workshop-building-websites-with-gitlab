---
title: "Host Jupyter Books in GitLab"
teaching: 0
exercises: 0
questions:
- "How do I publish a Jupyter book in web pages through GitLab?"
objectives:
- "Publish Jupyter notebooks on the web with GitHub Pages"
keypoints:
- "The GitLab pipeline allows us to deploy Jupyter books too"
---

So, now we have this local Jupyter Book that we want to set up remotely too. Another purpose that Git
was built for, in addition to versioning projects, is to synchronise versions of the same project in
multiple locations (your laptop and the one of your colleagues, or your laptop and a remote platform
like GitLab). Git is an invaluable tool for software development! If you are only learning about it
now, we suggest you look into it further.

For the purpose of this training, however, we will moslty use it through GitLab. Let's go back to your
homepage and let's create another empty project.

![New Project in GitLab](../fig/new-project--gitlab.png){: .image-with-shadow width="900px" }

This time, we will call it **My book website**. We will set it public and **DO NOT** initialise it with
a README. This is crucial. If we do so, indeed, the next page looks like:

![New Project in GitLab default output page](../fig/gitlab-empty-project-instructions.png){: .image-with-shadow width="900px" }

This page explains us how to **Push an existing folder**. It says we should:

```
  cd <existing_folder>
  git init --initial-branch=main
  git remote add origin https://gitlab.com/<your name>/my-book-website.git
  git add .
  git commit -m "Initial commit"
  git push --set-upstream origin main
```

We should paste all these instructions, one by one, again to our terminal. Start from our usual folder,
the parent one of the one where we have the book. Please notice that you will have to personalise the
paths reported above between the symbols `<>`.

If you followed all the steps above, you can re-open your gitlab project page and see that it changed
to:

![A Jupyter Book when pushed to Gitlab](../fig/jupyter-book-in-gitlab.png){: .image-with-shadow width="900px" }

What do we need now to tell GitLab what to do with it?
We need a `.gitlab-ci.yml` file indeed. You can add it through the `Edit > Web IDE` in the main
project folder and with the content:

~~~
pages:
  stage: deploy
  image: python:slim
  script:
    - pip install -U jupyter-book
    - jupyter-book clean .
    - jupyter-book build .
    - mv _build/html public
  artifacts:
    paths:
      - public
~~~
{: .language-yaml }

This piece of code:
1. Installs jupyter-book (or checks that it is correctly installed remotely).
2. Cleans the folder from files resulting from (eventual) previous builds.
3. runs the command `jupyter-book build .`, which builds the book in the folder into a `_build`
subfolder. You can check the output by running the same command in your terminal, and you will realise that the
actual HTML files are in the subfolder `_build/html`.
4. It then moves the HTML content into our usual `public` folder where the artifacts are stored.

Let's commit our changes as we did before, through the **Source Control** menu.
A good commit message here is `Add GitLab pipeline`. We don't need to create a new branch for it.
Once this is done, click the **Go to Project** button.

If you check now the `Build > Pipelines` page, there should be one pipeline running. We know that
it will generate a page, let's check `Deploy > Pages` to know where. Click on the link available there
to check the output!

> ## Your time to experiment with a template
> This template is deliberately minimal to give you the opportunity to test your documentation reading skills.
> Check the topic guides at [jupyterbook.org](https://jupyterbook.org/intro.html) and find a way to:
> 1. Add another page called "About" and linked from the table of contents.
> 2. Play with the file format of this new page, add the same type of content in MarkDown, reStructuredTex and Notebook formats.
> 3. Add one figure and a figure caption.
> 4. Insert a code cell. If you are familiar with any programming language, add a simple plot and visualise the output of such plot into your page.
> 5. For more advanced code features, check [how to make the code executable](https://jupyterbook.org/interactive/thebe.html)
> 6. Check the [gallery of Jupyter books](https://executablebooks.org/en/latest/gallery.html) for inspiration!
{: .challenge}
