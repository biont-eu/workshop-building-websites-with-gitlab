---
title: "Work with Jupyter books locally"
teaching: 0
exercises: 0
questions:
- "How do I use Jupyter books to generate a book-like website with chapters?"
objectives:
- "Use Jupyterbook to generate a project with chapters that can be written in MarkDown or in Jupyter Notebooks"
keypoints:
- "jupyter-book comes with a command to generate a simple version of a book, with a homepage and two others"
- "Through Jupyter books, you'll be able to integrate interactive components and code in your web pages"
---

Let's say, however, that your main goal for attending this training is not to develop an HTML
website for your group, but rather to host your code and deriving plots in a public page (e.g. for
sharing the result of an analysis with colleagues, to have a web and cool version of an infographic,
and much more).

For these purposes, we would advise you to look into the
[Jupyter](https://jupyter.org/) projects. From their website:
> Project Jupyter exists to develop open-source software, open-standards, and services for interactive computing across
> dozens of programming languages.

**Jupyter notebooks** allow you to create and share documents containing live code as well as narrative text.
**Jupyter Lab** expand their functionalities by creating and interactive development environment (allowing you to
navigate your file system and define the coding environment). **Jupyter Hub** is a multi-user version of Jupyer Lab
that institutions can implement locally.

And **Jupyer book**?
> Jupyter Book is an open source project for building beautiful, publication-quality books and documents from
> computational material.

The [extensive tutorial](https://jupyterbook.org/start/your-first-book.html) will guide you through the installation
steps and detailed customisation available,  in the context of this lesson we are going to cover just the basics.
First things first, let's install Jupyter Book in your local laptop.

If you came to these materials from a Carpentries workshop, you probably already have
`conda` installed as per [these setup instructions](https://swcarpentry.github.io/python-novice-inflammation/index.html#setup).
If that's the case, you can use in your terminal the command:

~~~
conda install -c conda-forge jupyter-book
~~~
{: .language-bash }

To install Jupyter-books.

An alternative is:

~~~
pip install -U jupyter-book
~~~
{: .language-bash }

Please note: you will need to have **pip** installed in this case. And overview of the installation
options is available [here](https://jupyterbook.org/en/stable/start/overview.html).

> ## When we say terminal...
>
> Because out of the scope of this lesson, we are not really describing what a terminal is, its language
> (also because there are multiple options) and its scope. You could learn more in 
> [this](https://swcarpentry.github.io/shell-novice/) Unix Shell lesson from the Carpentries.
>
> If you are following these materials as a follow-up of our BioNT Python workshop, you installed Jupyter Books
> either from the Anaconda Prompt (in this case through `pip`) or in the terminal (either through `conda` or `pip`).
> 
> Please run the following commands in whatever of the tow you used. 
{: .callout }


Now, let's create our first book project.

> ## Check where you are!
>
> Before running the following command, please check where you are on the terminal with the
> command `pwd`. Move outside of the home folder, possibly inside the Desktop, with `cd Desktop`.
>
{: .callout}

The `jupyter-book --help` command would help us in checking the options here, but this lesson will spoiler something: the

~~~
jupyter-book create jupyter-book-demo
~~~
{: .language-bash }

command will create a basic Jupyter book in a `jupyter-book-demo` folder. This folder already includes the three things
needed for building a book: a `_config.yml` file, a `_toc.yml` table of content and the book's content in a collection
of MarkDown, reStructuredText or Jupyter Notebook files.

Let's take a look at the `toc.yml` file. The content is:

~~~
# Table of contents
# Learn more at https://jupyterbook.org/customize/toc.html

format: jb-book
root: intro
chapters:
- file: markdown
- file: notebooks
~~~
{: .language-yaml }

From these instructions, we can maybe guess that the format is going to be a classic Jupyter book,
the home page is the `intro` file (in this case, `intro.md`), and then two additional pages are
going to be available as chapters: `markdown.md` and `notebooks.ipynb`. But how can we check our
understanding, and see the output?

In the same folder where you should still be in the terminal (the parent directory in which your book is), run:

~~~
jupyter-book build jupyter-book-demo
~~~
{: .language-bash }

This generates a long output that ends with:

~~~
Finished generating HTML for book.
Your book's HTML pages are here:
jupyter-book-demo/_build/html/
You can look at your book by opening this file in a browser:
jupyter-book-demo/_build/html/index.html
Or paste this line directly into your browser bar:
file:///<some file path>

===============================================================================
~~~
{: .language-bash }

Click on the link or copy the path at the end of this output in your browser to see the result. Indeed, the
home page is the content of `intro.md`, and the navigation menu on the left allows us to navigate between chapters, hence
access the **Markdown Files** and **Content with notebooks** pages. You may notice that this is much more graphically
refined than our previous HTML-only website, because Jupyter books come with styling tool that enrich our pages with
little effort from our side: the Markdown, reStructuredText or Jupyter Notebook formats are way easier to write than a
HTML one, and it's the JupyterBook that does the work of converting them to HTML for us (and to make them readable from
the browser). This is exactly what happened with the command `build`: if you are curious, you can check what is the content
of the folder `_build` that was just generated into our book folder. It includes a subfolder full of HTML files, right?

> ## Exercise: Add your notebook!
> Do you have any Jupyter Notebook with your code and plots? Add it to the folder you just created.
> Remember that if you add a file that you intend to be a chapter, you need to add it to the `toc.yml` file.
> If you don't have anything at disposal, no problem! Experiment a bit with editing the `markdown.md` file instead.
> Then, refresh your browser page to check the output.
>
{: .challenge }

So, now we have a book locally, but it still doesn't exist remotely and online. Stay tuned for how to add it to another
GitLab project and use GitLab to host it online!
