# Building Websites with GitLab

Lesson material, the rendered lesson is hosted
[here](https://workshop-building-websites-with-gitlab-biont-eu-2e49af9c9c94c62.gitlab.io/).

## Maintainer(s)

Current maintainer of this lesson is [Lisanna Paladin](https://bio-it.embl.de/lisanna-paladin/)

## Authors

A list of contributors to the lesson can be found in [AUTHORS](AUTHORS)

## Citation

To cite this lesson, please consult with [CITATION](CITATION)

## Development

To test and develop this lesson locally:

```
  bundle install
  bundle exec jekyll serve
```


[lesson-example]: https://carpentries.github.io/lesson-example
